# Belajar Git #

1. membuat repository

			git init nama-folder

2. Melihat status working folder

			git status

3. menyimpan perubahan ke staging area

			git add .

4. menyimpan perubahan ke repository

			git commit -m "pesan / keterangan commit"

5. melihat history perubahan

			git log

			git log --oneline

6. melihat perbedaan (difference) antar versi

			git diff commitId1..commitId2

7. membatalkan perubahan di staging 

			git reset

8. menghapus perubahan di working dan staging (!!!bahaya)

			git reset --hard

9. Mendaftarkan remote repository

			git remote add namaremote urlremote

			git remote add gitlab git@gitlab.com:diyaulhaqi/belajar-git.git

10. Mengambil repo dari remote

			git clone git@gitlab.com:diyaulhaqi/belajar-git.git

11. Mengupload repo local ke remote

			git push -u namaremote namabranch
			git push -u gitlab master

12. Meliaht daftar branch

			git branch --all
